import java.util.ArrayList;

public class Slowo {
    private String slowo = null;
    private final ArrayList<String> slowoDoDruku = new ArrayList<>();
    private int iloscBledow = 0;

    public String getSlowo() {
        return slowo;
    }

    public void setSlowo(String slowo1) {
        slowo = slowo1;
    }

    public boolean sprawdzLitere(String wprowadzonaLitera, Szubienica szubienica) {
        if (slowoDoDruku.contains(wprowadzonaLitera)) {
            System.out.println("Litera " + wprowadzonaLitera + " juz jest");
        } else if (slowo.contains(wprowadzonaLitera)) {
            System.out.println("jest " + wprowadzonaLitera);
            char[] zzz = wprowadzonaLitera.toCharArray();
            dopiszLitere(slowo, zzz[0]);

        } else {
            System.out.println("brak " + wprowadzonaLitera);
            iloscBledow++;
            szubienica.rysujSzubienice(iloscBledow);

        }
        return iloscBledow >= 11;
    }

    private void dopiszLitere(String slowo2, char litera) {
        for (int i = 0; i < slowo2.length(); i++)
            if (slowo2.charAt(i) == litera) {
                slowoDoDruku.set(i, Character.toString(litera));
            }
    }

    public void przygotujSlowoDoDruku(String slowoDD) {
        for (int i = 0; i < slowoDD.length(); i++) {
            slowoDoDruku.add(i, "_");

        }
    }

    public void drukujSlowo(ArrayList<String> slowoDoDruku) {

        for (String litera : slowoDoDruku) {
            System.out.print(litera);

        }
    }

    public ArrayList<String> getSlowoDoDruku() {
        return slowoDoDruku;
    }
}

