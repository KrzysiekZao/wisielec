public class GraWisielec {
    Szubienica szubienica = new Szubienica();
    private final Pomocnik pomocnik = new Pomocnik();
    public boolean koniec = false;

    private void przygotujGre() {
        szubienica.zbierzElemSzubienicy();
        System.out.println("Witaj w grze wisielec !!!");
        System.out.println("Podaj po jednej literze max 12 razy, zeby odgadnac szukane slowo.");
    }

    private void rozpocznijGre() {

        String szukaneSlowo = pomocnik.pobierzDaneWejsciowe("Podaj slowo: ");
        Slowo slowo = new Slowo();
        slowo.setSlowo(szukaneSlowo);
        slowo.przygotujSlowoDoDruku(slowo.getSlowo());

        System.out.println("Szukasz slowa skaldajacego sie z " + slowo.getSlowo().length() + " liter.");
        System.out.print("Odgadywane slowo: ");
        slowo.drukujSlowo(slowo.getSlowoDoDruku());
        while (!koniec) {
            System.out.println();
            String litera = pomocnik.pobierzDaneWejsciowe("Podaj litere: ");
            koniec = slowo.sprawdzLitere(litera, szubienica);
            System.out.print("Odgadywane slowo: ");
            slowo.drukujSlowo(slowo.getSlowoDoDruku());
            System.out.println(" ");
            if (!slowo.getSlowoDoDruku().contains("_")) {
                System.out.println("Zwyciestwo !!!");
                koniec = true;
            }


        }

    }

    private void zakonczGre() {
        System.out.println("Koniec gry !!!");
    }

    public static void main(String[] args) {
        GraWisielec gra = new GraWisielec();
        gra.przygotujGre();
        gra.rozpocznijGre();
        gra.zakonczGre();
    }
}
