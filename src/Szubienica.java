import java.util.ArrayList;

public class Szubienica {
    private final ArrayList<String> konstrukcjaSzubienicy = new ArrayList<>();
    private final ArrayList<String> elementySzubienicy = new ArrayList<>();


    public void zbierzElemSzubienicy() {
        elementySzubienicy.add("/"); //0
        elementySzubienicy.add("/ \\"); //1
        elementySzubienicy.add(" |"); //2
        elementySzubienicy.add(" |----"); //3
        elementySzubienicy.add(" |   |"); //4
        elementySzubienicy.add(" |   O"); //5
        elementySzubienicy.add(" |  /"); //6
        elementySzubienicy.add(" |  /|"); //7
        elementySzubienicy.add(" |  /|\\ "); //8
        elementySzubienicy.add(" |  /"); //9
        elementySzubienicy.add(" |  / \\ "); //10


    }

    public void rysujSzubienice(int iloscElemSzubienicy) {

        switch (iloscElemSzubienicy) {

            case 1 -> {
                for (int o = 0; o < 7; o++) {
                    konstrukcjaSzubienicy.add(" ");
                }
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, " ");
                konstrukcjaSzubienicy.set(2, " ");
                konstrukcjaSzubienicy.set(3, " ");
                konstrukcjaSzubienicy.set(4, " ");
                konstrukcjaSzubienicy.set(5, " ");
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(0));
            }
            case 2 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, " ");
                konstrukcjaSzubienicy.set(2, " ");
                konstrukcjaSzubienicy.set(3, " ");
                konstrukcjaSzubienicy.set(4, " ");
                konstrukcjaSzubienicy.set(5, " ");
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 3 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, " ");
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 4 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 5 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 6 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(5));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 7 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(5));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(6));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 8 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(5));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(7));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 9 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(5));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(8));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(2));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 10 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(5));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(8));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(9));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
            case 11 -> {
                konstrukcjaSzubienicy.set(0, " ");
                konstrukcjaSzubienicy.set(1, elementySzubienicy.get(3));
                konstrukcjaSzubienicy.set(2, elementySzubienicy.get(4));
                konstrukcjaSzubienicy.set(3, elementySzubienicy.get(5));
                konstrukcjaSzubienicy.set(4, elementySzubienicy.get(8));
                konstrukcjaSzubienicy.set(5, elementySzubienicy.get(10));
                konstrukcjaSzubienicy.set(6, elementySzubienicy.get(1));
            }
        }

        for (String konstrukcjaSzubienicy : konstrukcjaSzubienicy) {
            System.out.println(konstrukcjaSzubienicy);
        }
    }

}